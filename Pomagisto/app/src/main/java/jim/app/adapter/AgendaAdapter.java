/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jim.app.R;
import jim.app.model.Appointment;
import jim.app.viewholder.AppointmentViewHolder;
import rx.Observable;
import rx.subjects.PublishSubject;

public class AgendaAdapter extends RecyclerView.Adapter<AppointmentViewHolder> {

    private final PublishSubject<Appointment.Item> mClickedItemSubject;
    private final Context mContext;
    private List<Appointment> mAgenda;
    private int mLastPosition;

    public AgendaAdapter(Context context) {
        mClickedItemSubject = PublishSubject.create();
        mContext = context;
        mAgenda = new ArrayList<>();
        mLastPosition = -1;
    }

    @Override
    public AppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_agenda_item, parent, false);
            return new AppointmentViewHolder.Item(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_agenda_header, parent, false);
            return new AppointmentViewHolder.Header(view);
        }
    }

    @Override
    public void onBindViewHolder(AppointmentViewHolder viewHolder, int position) {
        Appointment appointment = mAgenda.get(position);
        viewHolder.setValues(appointment);
        setAnimation(viewHolder.itemView, position);

        // Only publish when an Appointment.Item is clicked
        if (appointment instanceof Appointment.Item) {
            viewHolder.itemView.setOnClickListener(view -> mClickedItemSubject.onNext((Appointment.Item) appointment));
        }
    }

    @Override
    public int getItemCount() {
        return mAgenda.size();
    }

    @Override
    public int getItemViewType(int position) {
        // There are two different kind of appointments:
        // * Appointment.Item
        // * Appointment.Header
        Appointment appointment = mAgenda.get(position);
        return appointment instanceof Appointment.Item ? 0 : 1;
    }

    @Override
    public void onViewDetachedFromWindow(final AppointmentViewHolder holder) {
        holder.itemView.clearAnimation();
        super.onViewDetachedFromWindow(holder);
    }

    public Observable<Appointment.Item> getClickedItemObservable() {
        return mClickedItemSubject.asObservable();
    }

    public void updateData(List<Appointment> agenda) {
        mAgenda.clear();
        mAgenda.addAll(agenda);
    }

    public void addData(Appointment appointment) {
        mAgenda.add(appointment);
    }

    public void addData(List<Appointment> appointments) {
        mAgenda.addAll(appointments);
    }

    public void clearData() {
        mAgenda.clear();
    }

    public void sortData() {
        Collections.sort(mAgenda);
    }

    public void filterData(String filter) {
        List<Appointment> filteredAgenda = new ArrayList<>();
        for (Appointment appointment : mAgenda) {
            if (appointment instanceof Appointment.Item) {
                Appointment.Item item = (Appointment.Item) appointment;
                if (item.getLecture().getFull().toLowerCase().equals(filter.toLowerCase())) {
                    filteredAgenda.add(appointment);
                }
            }
        }

        updateData(filteredAgenda);
    }

    private void setAnimation(View view, int position) {
        if (position > mLastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            view.startAnimation(animation);
            mLastPosition = position;
        }
    }

}
