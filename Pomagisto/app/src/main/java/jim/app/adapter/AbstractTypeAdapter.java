/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import jim.app.model.Appointment;

public class AbstractTypeAdapter implements JsonSerializer<Appointment>, JsonDeserializer<Appointment> {

    private static final String CLASSNAME = "CLASSNAME";
    private static final String INSTANCE  = "INSTANCE";

    @Override
    public JsonElement serialize(Appointment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject retValue = new JsonObject();
        String className = src.getClass().getName();
        retValue.addProperty(CLASSNAME, className);
        JsonElement elem = context.serialize(src);
        retValue.add(INSTANCE, elem);
        return retValue;
    }

    @Override
    public Appointment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
        String className = prim.getAsString();

        Class<?> klass = null;
        try {
            klass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new JsonParseException(e.getMessage());
        }

        return context.deserialize(jsonObject.get(INSTANCE), klass);
    }

}
