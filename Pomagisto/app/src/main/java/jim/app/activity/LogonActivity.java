/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jim.app.R;
import jim.app.model.Student;
import jim.app.util.SessionUtil;

public class LogonActivity extends AppCompatActivity {

    @BindView(R.id.text_school) EditText textSchool;
    @BindView(R.id.text_username) EditText textUsername;
    @BindView(R.id.text_password) EditText textPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logon);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_sign_in)
    public void logon(View view) {
        String school = textSchool.getText().toString();
        String username = textUsername.getText().toString();
        String password = textPassword.getText().toString();

        SessionUtil.createLoginSession(new Student(school, username, password));
        Intent intent = new Intent(LogonActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

}
