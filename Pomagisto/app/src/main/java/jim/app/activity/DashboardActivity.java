/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import butterknife.BindView;
import butterknife.ButterKnife;
import jim.app.R;
import jim.app.fragment.AgendaFragment;
import jim.app.fragment.GradeFragment;
import jim.app.fragment.OverviewFragment;
import jim.app.model.Student;
import jim.app.util.SessionUtil;

public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar mToolbar;
    private Student mAuth;
    private Drawer mDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        if (!SessionUtil.checkLoginSession()) {
            startLoginActivity();
            return;
        }

        mAuth = SessionUtil.Auth.getAuth();

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        initDrawer(savedInstanceState);

        showFragment(AgendaFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen()) mDrawer.closeDrawer();
        else if (agendaFragmentIsCurrentlyShown()) finish();
        else super.onBackPressed();
    }

    public void setSelection(long identifier) {
        mDrawer.setSelection(identifier, false);
    }

    public void clearSelection() {
        mDrawer.setSelection(-1);
    }

    public void showBackArrowIcon() {
        mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        if (getSupportActionBar()!= null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void showHamburgerIcon() {
        if (getSupportActionBar()!= null) getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
    }

    public void showFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment, fragment.getTag())
                .addToBackStack(null)
                .commit();
    }

    private void initDrawer(Bundle savedInstanceState) {
        // Create student profile
        final IProfile profile = new ProfileDrawerItem()
                .withName(mAuth.getUsername())
                .withEmail(mAuth.getSchool())
                .withIcon(R.drawable.profile5);

        // Create drawer header
        AccountHeader drawerHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.header)
                .withSelectionListEnabledForSingleProfile(false)
                .addProfiles(profile)
                .withSavedInstance(savedInstanceState)
                .build();

        // Create drawer
        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withHasStableIds(true)
                .withAccountHeader(drawerHeader)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.agenda_text).withIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_calendar, null)).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.overview_text).withIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_chart, null)).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.grades_text).withIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_grade, null)).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.sign_out_text).withIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_exit_app, null)).withIdentifier(4),
                        new SectionDrawerItem().withName(R.string.lectures_text),
                        new SecondaryDrawerItem().withName("Nederlands").withIdentifier(5),
                        new SecondaryDrawerItem().withName("Frans").withIdentifier(6),
                        new SecondaryDrawerItem().withName("Geschiedenis").withIdentifier(7),
                        new SecondaryDrawerItem().withName("Wiskunde").withIdentifier(8),
                        new SecondaryDrawerItem().withName("Lichamelijke opvoeding").withIdentifier(9),
                        new SecondaryDrawerItem().withName("Tekenen").withIdentifier(10),
                        new SecondaryDrawerItem().withName("Muziek").withIdentifier(11),
                        new SecondaryDrawerItem().withName("Techniek").withIdentifier(12)
                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    // Check if the drawerItem is set.
                    // There are different reasons for the drawerItem to be null
                    // * click on the header
                    // * click on the footer
                    if (drawerItem != null) {
                        drawerItem.isSelected();
                        switch ((int) drawerItem.getIdentifier()) {
                            case 1: showFragment(AgendaFragment.newInstance()); break;
                            case 2: showFragment(OverviewFragment.newInstance()); break;
                            case 3: showFragment(GradeFragment.newInstance()); break;
                            case 4:
                                SessionUtil.clearLoginSession();
                                startLoginActivity();
                                break;
                            default:
                                String filter = ((SecondaryDrawerItem) drawerItem).getName().getText();
                                int selection = position - 1;
                                showFragment(AgendaFragment.Filtered.newInstance(filter, selection));
                                break;
                        }
                    }

                    return false;
                })
                .withOnDrawerNavigationListener(clickedView -> {
                    DashboardActivity.this.onBackPressed();
                    return true;
                })
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();
    }

    private boolean agendaFragmentIsCurrentlyShown() {
        // Instance of AgendaFragment.class won't work here
        // because we also have the inner class AgendaFragment.Filtered
        return getSupportFragmentManager().findFragmentById(R.id.fragment_container).getClass() == AgendaFragment.class;
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LogonActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
