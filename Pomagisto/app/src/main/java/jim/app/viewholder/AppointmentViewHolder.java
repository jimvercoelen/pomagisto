package jim.app.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import jim.app.R;
import jim.app.util.DrawableUtil;
import jim.app.util.StringUtil;
import jim.app.model.Appointment;

// Base view holder class
public abstract class AppointmentViewHolder extends RecyclerView.ViewHolder {

    public AppointmentViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public abstract void setValues(Appointment appointment);

    // View holder class for an Appointment.Item
    public static class Item extends AppointmentViewHolder {

        @BindView(R.id.image_item_avatar) ImageView imageItemAvatar;
        @BindView(R.id.text_item_title) TextView textItemTitle;
        @BindView(R.id.text_item_time_span) TextView textItemTimeSpan;
        @BindView(R.id.text_item_sprint) TextView textItemSprint;

        public Item(View itemView) {
            super(itemView);
        }

        public void setValues(Appointment appointment) {
            Appointment.Item item = (Appointment.Item) appointment;
            textItemTitle.setText(item.getTitle());
            textItemTimeSpan.setText(item.getTimeSpan());
            textItemSprint.setText(item.getId());
            imageItemAvatar.setImageDrawable(DrawableUtil.writeTextOnAvatar(item.getLecture().getAbbreviated().toUpperCase()));
        }
    }

    // View holder class for an Appointment.Header
    public static class Header extends AppointmentViewHolder {

        @BindView(R.id.text_header_title) TextView textHeaderTitle;

        public Header(View itemView) {
            super(itemView);
        }

        public void setValues(Appointment appointment) {
            Appointment.Header header = (Appointment.Header) appointment;
            textHeaderTitle.setText(StringUtil.capitalizeFirstLetter(header.getTitle()));
        }
    }
}
