package jim.app.model;

import org.joda.time.DateTime;

import java.io.Serializable;

public abstract class Appointment implements Comparable<Appointment>, Serializable {

    protected abstract String getTitle();
    protected abstract DateTime getDateTime();

    @Override
    public int compareTo(Appointment other) {
        return getDateTime().compareTo(other.getDateTime());
    }

    public static class Item extends Appointment {

        private String _id;
        private Lecture lecture;
        private Teacher teacher;
        private String description;
        private String group;
        private String location;
        private String from;
        private String till;

        // TODO: remove on release
        public Item(String _id, Lecture lecture, Teacher teacher, String description, String group, String location, DateTime from, DateTime till) {
            this._id = _id;
            this.lecture = lecture;
            this.teacher = teacher;
            this.description = description;
            this.group = group;
            this.location = location;
            this.from = from.toString();
            this.till = till.toString();
        }

        @Override
        public String getTitle() {
            return teacher.getAbbreviated() + " - " + location;
        }

        @Override
        public DateTime getDateTime() {
            return DateTime.parse(from);
        }

        public String getId() {
            return _id;
        }

        public Lecture getLecture() {
            return lecture;
        }

        public Teacher getTeacher() {
            return teacher;
        }

        public String getDescription() {
            return description;
        }

        public String getGroup() {
            return group;
        }

        public String getLocation() {
            return location;
        }

        public String getTimeSpan() {
            return DateTime.parse(from).toString("kk:mm") + " - " + DateTime.parse(till).toString("kk:mm");
        }
    }

    public static class Header extends Appointment {

        private String title;
        private DateTime dateTime;

        public Header(String title, DateTime dateTime) {
            this.title = title;
            this.dateTime = dateTime;
        }

        @Override
        public String getTitle() {
            return title;
        }

        @Override
        public DateTime getDateTime() {
            return dateTime;
        }
    }
}
