/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.model;

import java.util.ArrayList;
import java.util.List;

public class TimeStamp {

    private static long counter = 0;
    private long curBegin;
    private String curBeginS;
    private List<Period> list;

    public TimeStamp() {
        TimeStamp.counter = 0;
        this.curBegin = 0;
        this.curBeginS = null;
        this.list = new ArrayList<>();
    }

    public void setBegin() {
        this.setBegin(String.valueOf(TimeStamp.counter++));
    }

    public void setBegin(String timepoint) {
        this.curBegin = System.currentTimeMillis();
        this.curBeginS = timepoint;
    }

    public void setEnd() {
        this.setEnd(String.valueOf(TimeStamp.counter++));
    }

    public void setEnd(String timepoint) {
        this.list.add(new Period(this.curBegin, this.curBeginS, System.currentTimeMillis(), timepoint));
    }

    public void setEndBegin(String timepoint) {
        this.setEnd(timepoint);
        this.setBegin(timepoint);
    }

    private class Period {

        long begin;
        String beginS;
        long end;
        String endS;

        public Period(long b, String sb, long e, String se) {
            this.begin = b;
            this.beginS = sb;
            this.end = e;
            this.endS = se;
        }

        @Override
        public String toString() {
            return "From '" + this.beginS + "' till '" + this.endS + "' is " + (this.end - this.begin) + " mSec.";
        }
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Period p : this.list) {
            builder.append(p.toString());
            builder.append('\n');
        }

        return builder.toString();
    }
}
