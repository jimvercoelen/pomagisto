/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.model;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private String _id;
    private String username;
    private String password;
    private String school;
    private String group;
    private List<Appointment> agenda = new ArrayList<>();

    public Student(String school, String username, String password) {
        this.school = school;
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return _id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSchool() {
        return school;
    }

    public String getGroup() {
        return group;
    }

    public boolean agendaIsCached() {
        return agenda.size() > 0;
    }

    public List<Appointment> getAgenda() {
        if (agenda == null) agenda = new ArrayList<>();
        return agenda;
    }

    public void setAgenda(List<Appointment> appointments) {
        if (agenda == null) agenda = new ArrayList<>();
        agenda.addAll(appointments);
    }

    public void addAppointment(Appointment appointments) {
        if (agenda == null) agenda = new ArrayList<>();
        agenda.add(appointments);
    }

}
