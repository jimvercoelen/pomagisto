/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jim.app.R;
import jim.app.activity.DashboardActivity;
import jim.app.model.Appointment;
import jim.app.util.StringUtil;

public class AppointmentFragment extends Fragment {

    private static final String ITEM_KEY = "item";

    @BindView(R.id.text_item_title) TextView textItemTitle;
    @BindView(R.id.text_item_time_span) TextView textItemTimeSpan;
    @BindView(R.id.text_item_sprint) TextView textItemSprint;
    @BindView(R.id.text_item_description) TextView textItemDescription;
    @BindView(R.id.scroll_container) ScrollView scrollContainer;
    @BindView(R.id.seekbar_duration) SeekBar seekBarDuration;

    private Appointment.Item mItem;
    private DashboardActivity mActivity;
    private Unbinder mUnbinder;

    /**
     * Static factory method that takes ONLY one appointment item as parameter,
     * initializes the fragment's arguments, and returns the new fragment.
     * @param item                  an appointment item
     * @return                      new AgendaItemFragment
     */
    public static AppointmentFragment newInstance(Appointment.Item item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ITEM_KEY, item);
        AppointmentFragment fragment = new AppointmentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (DashboardActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appointment, container, false);

        mItem = (Appointment.Item) getArguments().getSerializable(ITEM_KEY);
        mUnbinder = ButterKnife.bind(this, view);

        initToolbar();

        initViews();

        return view;
    }

    private void initToolbar() {
        mActivity.setTitle(StringUtil.capitalizeFirstLetter(mItem.getLecture().getFull()));
        mActivity.showBackArrowIcon();
        mActivity.clearSelection();
    }

    private void initViews() {
        textItemTitle.setText(mItem.getTitle());
        textItemTimeSpan.setText(mItem.getTimeSpan());
        textItemSprint.setText(mItem.getId());
        String description = mItem.getDescription() + " " + mItem.getDescription() + " " + mItem.getDescription();
        textItemDescription.setText(description);
        scrollContainer.setFadingEdgeLength(25);
        //seekBarDuration.setPadding(0, 0, 0, 0);
        seekBarDuration.setProgress(1);
        seekBarDuration.setMax(8);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

}
