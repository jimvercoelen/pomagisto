/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jim.app.util.DateTimeUtil;
import jim.app.R;
import jim.app.activity.DashboardActivity;
import jim.app.model.Appointment;
import jim.app.model.Student;
import jim.app.adapter.AgendaAdapter;
import jim.app.util.SessionUtil;
import jim.app.util.StringUtil;

public class AgendaFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.agenda_list) RecyclerView mList;
    @BindView(R.id.agenda_swipe) SwipeRefreshLayout mSwipeLayout;

    protected DashboardActivity mActivity;
    protected Student mAuth;
    protected AgendaAdapter mAdapter;
    protected Unbinder mUnbinder;
    protected DateTime mStartDate;
    protected DateTime mEndDate;

    public static AgendaFragment newInstance(Object ...args) {
        return new AgendaFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (DashboardActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agenda, container, false);

        initToolbar();

        mUnbinder = ButterKnife.bind(this, view);
        mStartDate = DateTimeUtil.calculatePreviousMonday(null);
        mEndDate = DateTimeUtil.calculateNextSunday(null);
        mAuth = SessionUtil.Auth.getAuth();
        mAdapter = new AgendaAdapter(getContext());
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeColors(
                ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null),
                ResourcesCompat.getColor(getResources(), R.color.colorPrimaryDark, null));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setLayoutManager(layoutManager);
        mList.setAdapter(mAdapter);

        handleClickedAppointmentItem();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initAgendaContent();
    }

    @Override
    public void onRefresh() {
        initAgendaContent();
        mSwipeLayout.setRefreshing(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    protected void initToolbar() {
        mActivity.setTitle(getString(R.string.agenda_text));
        mActivity.showHamburgerIcon();
        mActivity.setSelection(1);
    }

    protected void handleClickedAppointmentItem() {
        mAdapter.getClickedItemObservable()
                .subscribe(item -> mActivity.showFragment(AppointmentFragment.newInstance(item)));
    }

    protected void initAgendaContent() {
        mAdapter.clearData();
        initAgendaItems();
        initAgendaHeaders();
        mAdapter.sortData();
        mAdapter.notifyDataSetChanged();
    }

    protected void initAgendaItems() {
        mAdapter.addData(mAuth.getAgenda());
    }

    protected void initAgendaHeaders() {
        int daysBetween = DateTimeUtil.getDaysBetween(mStartDate, mEndDate) + 1;
        for (int i = 0; i < daysBetween; i ++) {
            DateTime dateTime = mStartDate.plusDays(i);
            String dayOfWeek = DateTimeUtil.getLocaleDayOfWeekAsText(dateTime);
            mAdapter.addData(new Appointment.Header(dayOfWeek, dateTime));
        }
    }

    /**
     * To keep things separated from each other, this static
     * inner class extending the AgendaFragment was made.
     * The only differences are the toolbar configuration
     * and filtered data.
     */
    public static class Filtered extends AgendaFragment {

        private static final String FILTER_KEY = "filter";
        private static final String SELECTION_KEY = "selection";

        /**
         * Static method that takes Object ...args as parameter,
         * initializes the fragment's arguments and returns the fragment.
         * @param args              Filter on index 0 and
         *                          selection on index 1 is required!
         * @return                  new AgendaFragment.Filtered
         */
        public static AgendaFragment.Filtered newInstance(Object ...args) {
            Bundle bundle = new Bundle();
            bundle.putString(FILTER_KEY, (String) args[0]);
            bundle.putInt(SELECTION_KEY, (int) args[1]);
            AgendaFragment.Filtered fragment = new AgendaFragment.Filtered();
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        protected void initToolbar() {
            mActivity.setTitle(StringUtil.capitalizeFirstLetter(getArguments().getString(FILTER_KEY)));
            mActivity.showBackArrowIcon();
            mActivity.setSelection(getArguments().getInt(SELECTION_KEY));
        }

        @Override
        protected void initAgendaItems() {
            super.initAgendaItems();
            mAdapter.filterData(getArguments().getString(FILTER_KEY));
        }
    }
}
