/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.util;

import android.content.SharedPreferences;

import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import jim.app.App;
import jim.app.adapter.AbstractTypeAdapter;
import jim.app.model.Appointment;
import jim.app.model.Lecture;
import jim.app.model.Student;
import jim.app.model.Teacher;

public class SessionUtil {

    private static final String PREF_NAME = "PomagistoPref";
    private static final String KEY_AUTH = "auth";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final int PRIVATE_MODE = 0;

    private static SharedPreferences mPref;
    private static SharedPreferences.Editor mEditor;
    private static GsonBuilder mGsonBuilder;

    // TODO: create singleton and provide from module
    static {
        mPref = App.getContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        mEditor = mPref.edit();

        // A custom type adapter is required for (de)serializing
        // abstract Appointment class
        mGsonBuilder = new GsonBuilder();
        mGsonBuilder.registerTypeAdapter(Appointment.class, new AbstractTypeAdapter());
    }

    /**
     * Saves user to shared preferences
     * and sets the authenticated user.
     * @param auth      authenticated user
     */
    public static void createLoginSession(Student auth) {
        Auth.mAuth = auth;
        insertDummyAgenda();
        mEditor.putBoolean(IS_LOGIN, true);
        mEditor.putString(KEY_AUTH, mGsonBuilder.create().toJson(auth));
        mEditor.apply();        // apply will run in background
    }

    /**
     * Checks for a saved user from shared preferences
     * and retrieves if there is one.
     * @return              true when user is found
     */
    public static boolean checkLoginSession() {
        if (!isLoggedIn()) return false;
        retrieve();
        return true;
    }

    /**
     * Removes saved user from shared preferences
     */
    public static void clearLoginSession() {
        mEditor.clear();
        mEditor.apply();        // apply will run in background
    }

    private static void retrieve() {
        String json = mPref.getString(KEY_AUTH, null);
        Auth.mAuth = mGsonBuilder.create().fromJson(json, Student.class);
    }

    private static boolean isLoggedIn() {
        return mPref.getBoolean(IS_LOGIN, false);
    }

    // TODO: remove on release
    private static void insertDummyAgenda() {
        Auth.mAuth.getAgenda().add(new Appointment.Item("1", new Lecture("wiskunde", "wi"), new Teacher("M. van Krieken", "KRE"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B201", new DateTime().withHourOfDay(8), new DateTime().withHourOfDay(10)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("2", new Lecture("lichamelijke opvoeding", "lo"), new Teacher("G.R. Botma", "BOA"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B111", new DateTime().withHourOfDay(10), new DateTime().withHourOfDay(11)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("3", new Lecture("techniek", "te"), new Teacher("A.E. Roosenboom", "ROO"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B204", new DateTime().withHourOfDay(11), new DateTime().withHourOfDay(12)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("4", new Lecture("mentorles", "me"), new Teacher("M. van Krieken", "KRE"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B204", new DateTime().withHourOfDay(13), new DateTime().withHourOfDay(14)));

        Auth.mAuth.getAgenda().add(new Appointment.Item("5", new Lecture("Engels", "en"), new Teacher("S.W.H. de Vaal", "VAA"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "M120", new DateTime().plusDays(1).withHourOfDay(8), new DateTime().plusDays(1).withHourOfDay(9)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("6", new Lecture("aadrijkskunde", "ak"), new Teacher("K. Boer", "DOR"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B201", new DateTime().plusDays(1).withHourOfDay(10), new DateTime().plusDays(1).withHourOfDay(11)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("7", new Lecture("Nederlands", "nl"), new Teacher("E. van Doesburg", "DOE"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B201", new DateTime().plusDays(1).withHourOfDay(11), new DateTime().plusDays(1).withHourOfDay(12)));

        Auth.mAuth.getAgenda().add(new Appointment.Item("8", new Lecture("wiskunde", "wi"), new Teacher("M. van Krieken", "KRE"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B201", new DateTime().plusDays(2).withHourOfDay(8), new DateTime().plusDays(2).withHourOfDay(9)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("9", new Lecture("mentorles", "me"), new Teacher("M. van Krieken", "KRE"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B204", new DateTime().plusDays(2).withHourOfDay(9), new DateTime().plusDays(2).withHourOfDay(10)));

        Auth.mAuth.getAgenda().add(new Appointment.Item("10", new Lecture("Engels", "en"), new Teacher("S.W.H. de Vaal", "VAA"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "M120", new DateTime().plusDays(3).withHourOfDay(8), new DateTime().plusDays(3).withHourOfDay(9)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("11", new Lecture("aadrijkskunde", "ak"), new Teacher("K. Boer", "DOR"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B201", new DateTime().plusDays(3).withHourOfDay(10), new DateTime().plusDays(3).withHourOfDay(11)));
        Auth.mAuth.getAgenda().add(new Appointment.Item("12", new Lecture("Nederlands", "nl"), new Teacher("E. van Doesburg", "DOE"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.", "2TB", "B201", new DateTime().plusDays(3).withHourOfDay(11), new DateTime().plusDays(3).withHourOfDay(12)));
    }

    /**
     * Static inner class holding the authenticated user.
     * Using this class allows us to get the authenticated user
     * anytime, anywhere throughout the entire app.
     * Setting the authenticated user can only be done by SessionUtil.
     */
    public static class Auth {

        private static Student mAuth;

        public static Student getAuth() {
            return mAuth;
        }
    }
}
