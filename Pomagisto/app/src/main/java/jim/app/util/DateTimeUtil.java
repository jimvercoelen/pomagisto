/*
 * Copyright (C) 2016 Pomagisto
 *
 * Licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 *      https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 *
 * You are free to:
 *      Share — copy and redistribute the material in any medium or format
 *      The licensor cannot revoke these freedoms as long as you follow the license terms.
 *
 * Under the following terms:
 *      Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 *      NonCommercial — You may not use the material for commercial purposes.
 *      NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
 *      No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
 */
package jim.app.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;

import java.util.Locale;

public class DateTimeUtil {

    public static DateTime calculateNextSunday(DateTime dateTime) {
        if (dateTime == null) dateTime = new DateTime();
        return dateTime.withHourOfDay(23).withMinuteOfHour(59).withDayOfWeek(DateTimeConstants.SUNDAY);
    }

    public static DateTime calculatePreviousMonday(DateTime dateTime) {
        if (dateTime == null) dateTime = new DateTime();
        return dateTime.withHourOfDay(0).withMinuteOfHour(0).withDayOfWeek(DateTimeConstants.MONDAY);
    }

    public static String getLocaleDayOfWeekAsText(DateTime dateTime) {
        return dateTime.dayOfWeek().getAsText(Locale.getDefault());
    }

    public static int getDaysBetween(DateTime start, DateTime end) {
        return Days.daysBetween(start, end).getDays();
    }
}
