package jim.app.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextPaint;

import jim.app.App;
import jim.app.R;

public class DrawableUtil {

    private static final String ROBOTO_REGULAR = "sans-serif";
    private static final int AVATAR_TEXT_SIZE = 36;
    private static final int CANVAS_WIDTH = 80;
    private static final int CANVAS_HEIGHT = 80;

    private static Drawable mAvatarDrawable;
    private static Paint mAvatarTextPaint;

    /**
     * There is no need to create the drawable and paint variables
     * every single time writeTextOnAvatar is called.
     * That's why these variables are instantiated on class creation.
     * (Just ignore 'may produce NullPointerException')
     */
    static {
        mAvatarDrawable = ResourcesCompat.getDrawable(App.getContext().getResources(), R.drawable.list_item_circle, null);
        mAvatarDrawable.setBounds(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        mAvatarTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mAvatarTextPaint.setStyle(Paint.Style.FILL);
        mAvatarTextPaint.setTypeface(Typeface.create(ROBOTO_REGULAR, Typeface.NORMAL));
        mAvatarTextPaint.setColor(Color.rgb(33, 33, 33));
        mAvatarTextPaint.setTextAlign(Paint.Align.CENTER);
        mAvatarTextPaint.setTextSize(AVATAR_TEXT_SIZE);
    }

    public static BitmapDrawable writeTextOnAvatar(String text) {
        Bitmap bitmap = Bitmap.createBitmap(CANVAS_WIDTH, CANVAS_HEIGHT, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        mAvatarDrawable.draw(canvas);

        // Get position so it's in the center of the drawable
        int xPos = (canvas.getWidth() / 2);
        int yPos = (int) ((canvas.getHeight() / 2) - ((mAvatarTextPaint.descent() + mAvatarTextPaint.ascent()) / 2));
        canvas.drawText(text, xPos, yPos, mAvatarTextPaint);

        return new BitmapDrawable(App.getContext().getResources(), bitmap);
    }
}
