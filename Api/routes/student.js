var express = require('express');
var Student = require('../models/student');
var router = express.Router();

router.post('/auth', function (req, res, next) {
  console.log(req.body);

  Student.findOne({
    'username': req.body.username,
    'password': req.body.password,
    'school': req.body.school.toLowerCase()
  }).select('-password')
    .exec(function (error, student) {
      if (error) {
        next(error);
      } else if (!student) {
        res.status(500).send('Not found');
      } else {
        res.send(student);
      }
    });
});

router.post('/student', function (req, res, next) {
  var newStudent = req.body;

  Student.create(newStudent, function (error, student) {
    if (error) {
      next(error);
    } else {
      res.send(student);
    }
  });
});

module.exports = router;
