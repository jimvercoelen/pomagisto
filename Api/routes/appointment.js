var express = require('express');
var Appointment = require('../models/appointment');
var router = express.Router();

router.get('/appointments/:group', function (req, res, next) {
  Appointment.find({ group: req.params.group })
    .exec(function (error, appointments) {
      if (error) {
        next(error);
      } else {

        sleep(3000);

        res.send(appointments);
      }
    })
});

module.exports = router;

function sleep(miliseconds) {
  var currentTime = new Date().getTime();

  while (currentTime + miliseconds >= new Date().getTime()) {
    // sleep
  }
}
