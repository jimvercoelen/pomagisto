var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var appointmentSchema = mongoose.model('Appointment', new Schema({
  lecture: {
    full: {
      type: String
    },
    abbreviated: {
      type: String
    }
  },
  teacher: {
    name: {
      type: String
    },
    abbreviated: {
      type: String
    }
  },
  description: {
    type: String
  },
  group: {
    type: String
  },
  location: {
    type: String
  },
  from: {
    type: Date
  },
  till: {
    type: Date
  }
}), 'Appointment');

module.exports = appointmentSchema;
