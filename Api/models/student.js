var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var studentSchema = mongoose.model('Student', new Schema({
  username: {
    type: String
  },
  password: {
    type: String
  },
  school: {
    type: String
  },
  group: {
    type: String
  }
}), 'Student');

module.exports = studentSchema;
