var async = require('async');
var initializeDatabase = require('./initializers/database');
var initializeServer = require('./initializers/server');

function start () {
  async.series([
    initializeServer,
    initializeDatabase
  ], function (error) {
    if (error) {
      console.log('Error: ' + error);
    } else {
      console.log('API is running');
    }
  });
}

start();
