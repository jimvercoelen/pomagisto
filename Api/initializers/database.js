var mongoose = require('mongoose');
var config = require('../config.json');
var mongoUri = config.mongoUri + config.dbName;

module.exports = function initializeDatabase (callback) {
  mongoose.connect(mongoUri, function (error) {
    if (error) {
      return callback(error);
    }

    console.log('Database connection open');
    callback();
  })
};
