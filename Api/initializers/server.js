var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');

var config = require('../config.json');
var loadRoutes = require('../libs/load-routes');
var errorHandler = require('../middleware/error-handler');

var app = express();

const ROOT_DIR = __dirname + '/..';
const ROUTES_DIR = ROOT_DIR + '/routes';

app.disable('x-powered-by');
app.use(cors());
app.use(bodyParser.json());

loadRoutes(ROUTES_DIR, app);

app.use(errorHandler);

module.exports = function initializeServer (callback) {
  app.listen(config.port, function (error) {
    if (error) {
      return callback(error);
    }

    console.log('Server running on port: ' + config.port);
    callback();
  })
};
