module.exports = function () {
  return function (error, req, res, next) {
    res.status(error.status || 500);

    res.json({
      message: error.message,
      error: error
    });
  }
};
