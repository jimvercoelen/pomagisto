var requireDir = require('require-dir');

module.exports = function loadRoutes (routesDir, app) {
  var routes = requireDir(routesDir, {
    recurse: true
  });

  Object.keys(routes).forEach(function (route) {
    var router = routes[route];

    if (typeof router !== 'function') {
      throw new Error('Route file: routes/' + route + '.js did not return a router.');
    }

    app.use(router);
  })
};
