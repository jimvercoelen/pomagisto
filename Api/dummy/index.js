var async = require('async');
var initializeDatabase = require('../initializers/database');
var clearCollections = require('./clear');
var createAppointments = require('./appointment');

initializeDatabase(function () {

  async.series([
    clearCollections,
    createAppointments
  ], function (error, appointments) {
    if (error) {
      console.log(error);
    } else {
      console.log(appointments);
      console.log('Done inserting dummy data');
    }
  });
});
