var Appointment = require('../models/appointment');

module.exports = function clearCollections(callback) {
  Appointment.remove({}, function (error) {
    callback(error);
  });
};
