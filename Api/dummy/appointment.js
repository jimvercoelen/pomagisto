var moment = require('moment');

var Appointment = require('../models/appointment');
var dummyAppointments = [];

function pushAppointment (lecture, teacher, description, location, from, till) {
  dummyAppointments.push({
    lecture: lecture,
    teacher: teacher,
    description: description,
    location: location,
    group: '2TB',
    from: from,
    till: till
  })
}

module.exports = function createAppointments (callback) {
  pushAppointment(
    { full: 'wiskunde', abbreviated: 'wi' },
    { name: 'M. van Krieken', abbreviated: 'KRE' },
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.',
    'B201',
    moment('06:30 +0000', 'HH:mm Z').day('Monday'),
    moment('08:30 +0000', 'HH:mm Z').day('Monday')
  );

  pushAppointment(
    { full: 'wiskunde', abbreviated: 'wi' },
    { name: 'M. van Krieken', abbreviated: 'KRE' },
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae nunc in lectus condimentum rhoncus. Nunc mattis at mauris in dapibus. Donec sit amet ligula nec quam posuere placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis in vestibulum augue. Integer in enim dolor. Nunc in rutrum ligula. Vivamus efficitur egestas diam at scelerisque.',
    'B201',
    moment('08:30 +0000', 'HH:mm Z').day('Monday'),
    moment('09:40 +0000', 'HH:mm Z').day('Monday')
  );

  pushAppointment(
    { full: 'lichamelijke opvoeding', abbreviated: 'lo' },
    { name: 'A.E. Roosenboom', abbreviated: 'ROO' },
    'Vestibulum vitae commodo velit. Pellentesque ultrices porttitor rutrum. Vestibulum commodo laoreet nisl at condimentum. Sed vehicula vitae tortor ut eleifend. Pellentesque non porta quam. Cras aliquam ligula urna, id auctor libero vehicula quis. Fusce interdum porta arcu, non venenatis mauris consectetur a. In eu ante eget enim aliquam placerat sed sit amet ipsum. Morbi imperdiet mauris felis, nec egestas risus aliquam sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent interdum rutrum metus, ut luctus lacus iaculis ac. In gravida libero vel sem iaculis, sit amet malesuada elit accumsan. Nullam ut tortor quis est eleifend ultrices',
    'B111',
    moment('08:30 +0000', 'HH:mm Z').day('Tuesday'),
    moment('09:40 +0000', 'HH:mm Z').day('Tuesday')
  );

  pushAppointment(
    { full: 'techniek', abbreviated: 'tn' },
    { name: 'G.R. Botma', abbreviated: 'BOA' },
    'In sodales feugiat nunc, sit amet volutpat sem auctor a. Etiam vel mauris et erat sagittis malesuada. Quisque a leo vel mauris porttitor ornare sit amet ut ligula. Maecenas facilisis interdum luctus. Praesent lacus tortor, pharetra vel magna in, aliquet ornare metus. Vivamus pellentesque odio quis leo vestibulum imperdiet. Nulla sagittis eget neque ac molestie.',
    'B204',
    moment('09:40 +0000', 'HH:mm Z').day('Tuesday'),
    moment('10:50 +0000', 'HH:mm Z').day('Tuesday')
  );

  pushAppointment(
    { full: 'mentorles', abbreviated: 'ml' },
    { name: 'M. van Krieken', abbreviated: 'KRE' },
    'Aenean non odio tristique, luctus leo a, suscipit nunc. Donec ut mattis nibh. Curabitur semper enim quis lobortis dictum. Morbi nec nunc viverra, tincidunt velit quis, consequat lacus. In hac habitasse platea dictumst. Nullam eget lectus malesuada, cursus purus sit amet, porttitor risus. Fusce eleifend diam vitae sapien accumsan, vitae mollis dui sollicitudin. Aliquam erat volutpat. Proin vitae mi in ligula laoreet aliquet non a ex. Proin orci lectus, scelerisque in metus eget, rutrum bibendum ante. Pellentesque ut magna dui. Sed porttitor porta leo, ac ornare nulla placerat et.',
    'B201',
    moment('09:40 +0000', 'HH:mm Z').day('Wednesday'),
    moment('10:50 +0000', 'HH:mm Z').day('Wednesday')
  );

  pushAppointment(
    { full: 'Engels', abbreviated: 'en' },
    { name: 'S.W.H. de Vaal', abbreviated: 'VAA' },
    'In sollicitudin, elit in aliquet tempus, elit dolor hendrerit dolor, ut rhoncus metus quam ac diam. Suspendisse eu lacus luctus, sodales neque ut, egestas justo. Aliquam lacinia at lorem a convallis. Aenean tincidunt tempus est, quis faucibus tellus porttitor commodo. Aenean molestie sed risus ac faucibus. Morbi in vulputate ligula. Fusce feugiat ipsum non dapibus condimentum. Sed condimentum cursus condimentum. Vestibulum varius facilisis augue vel tristique. Cras sagittis faucibus libero, in gravida mauris blandit non. Morbi tristique mauris lorem, id auctor massa tincidunt condimentum. Duis at malesuada eros.',
    'M120',
    moment('14:00 +0000', 'HH:mm Z').day('Wednesday'),
    moment('15:10 +0000', 'HH:mm Z').day('Wednesday')
  );

  pushAppointment(
    { full: 'aadrijkskunde', abbreviated: 'ak' },
    { name: 'K. Boer', abbreviated: 'DOR' },
    'Maecenas at augue nisi. Quisque rhoncus tempor scelerisque. Donec commodo purus ac diam mollis viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam vehicula blandit neque, non tincidunt lectus pellentesque a. Maecenas tincidunt eleifend velit eget interdum. Aliquam vel pharetra sem.',
    'B201',
    moment('08:30 +0000', 'HH:mm Z').day('Friday'),
    moment('09:40 +0000', 'HH:mm Z').day('Friday')
  );

  pushAppointment(
    { full: 'Nederlands', abbreviated: 'nl' },
    { name: 'E. van Doesburg', abbreviated: 'DOE' },
    'Morbi facilisis porttitor lobortis. Aliquam erat volutpat. Aliquam blandit mi sed tempus lobortis. Nunc volutpat elementum pharetra.',
    'M120',
    moment('11:10 +0000', 'HH:mm Z').day('Friday'),
    moment('12:20 +0000', 'HH:mm Z').day('Friday')
  );

  Appointment.create(dummyAppointments, callback);
};
